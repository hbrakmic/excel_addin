import { oDataResource } from 'jinqu-odata'

@oDataResource("People")
@oDataResource('Person')
export class Person {
    UserName: string
    FirstName: string
    LastName: string
}
