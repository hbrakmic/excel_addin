### PatentSight Excel AddIn

This is an [Office.js](https://docs.microsoft.com/en-us/office/dev/add-ins/) Excel Add-In project.

#### Installation

* `npm install` or `pnpm install`

#### Running

* Developer mode: `npm run start:desktop` or `pnpm start:desktop`

![addin_loaded](./assets/addin_loaded.png)

*Click the addin button in the upper right corner to open the task pane*

![addin_opened](./assets/addin_opened.png)

*Click Query OData Service* to load data from [TripPin OData Service](https://www.odata.org/blog/trippin-new-odata-v4-sample-service/)

![addin_executed](./assets/addin_executed.png)

#### Packages

This project uses [linqu-odata](https://www.npmjs.com/package/jinqu-odata) to query remote OData services.
