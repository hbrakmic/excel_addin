// images references in the manifest
import '../../assets/icon-16.png';
import '../../assets/icon-32.png';
import '../../assets/icon-80.png';
import { Person } from '../models/patentsight';
import { ODataService } from 'jinqu-odata'
let notifications: HTMLElement; // for debug info only

const odataSvcUrl = 'https://services.odata.org/V4/%28S%28wptr35qf3bz4kb5oatn432ul%29%29/TripPinServiceRW';

/* global console, document, Excel, Office */
Office.onReady(info => {
  if (info.host === Office.HostType.Excel) {
    document.getElementById("sideload-msg").style.display = "none";
    document.getElementById("app-body").style.display = "flex";
    document.getElementById("queryOdata").onclick = queryOdata;
    notifications = document.getElementById('notifications');
  }
});

// callback for querying odata
export async function queryOdata() {
  try {
    await Excel.run(async context => {

      // get a range (which is basically a marked collection of columns with a human-readable name)
      // instead of using A4:L4 for example we can simply say "Customers"
      const range = context.workbook.names.getItem('Customers').getRange();
 
      // initialize communication with our OData service
      const odataSvc = new ODataService (odataSvcUrl);
      // use above object definitio to map to odata values properly
      const customers = await odataSvc.createQuery(Person)
                                     .toArrayAsync()

      // Read the range Customers
      range.load("Customers");

      // add a new table
      // the boolean true indicates that this table has column headers
      var table = context.workbook.tables.add("B4:D4", true);
        
      // iterate over retrieved customer objects and add them to this table
      // notice: we must use 2-dimensional arrays here
      // notice 2: the "null" argument stands for 'add next line to the end of the table'
      customers.forEach((c: Person, i: number, a: any[]) => {
        table.rows.add(null, [
          [c.UserName, c.FirstName, c.LastName]
        ])
      });
      
      // ultimately, let Excel update the sheet
      await context.sync();
    });
  } catch (error) {
    showNotification(JSON.stringify(error));
  }
}

const showNotification = (message) => {
  notifications.innerText = message;
}
